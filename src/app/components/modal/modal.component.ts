import { Component, Input, Output, EventEmitter  } from '@angular/core';
import { Dish } from 'src/app/models/dish.model';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})

export class ModalComponent {
  @Input() dish: Dish;
  @Output() closeEvent = new EventEmitter<any>();
  constructor() { }
  public closeModal(): void {
    this.closeEvent.emit();
  }
}
