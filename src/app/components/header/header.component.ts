import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor(private router: Router, private authService: AuthService) { }
  public redirectToChannels(): void {
    this.router.navigate(['channels']);
  }
  public get isChannelsActive(): boolean {
    const url = this.router.url;
    return url.includes('channels');
  }

  public get isAuth(): boolean {
    return this.authService.isLogin;
  }
  public get username(): string {
    return this.authService.name;
  }

  public logout(): void {
    this.authService.logout();
    this.router.navigate(['login']);
  }
}
