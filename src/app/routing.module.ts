import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { MenuPageComponent } from './pages/menu/menu.component';
import { ChannelsPageComponent } from './pages/chanenels/chanenels.component';
import { AboutComponent } from './pages/about/about.component';
import { LoginPageComponent } from './pages/login/login.component';
import { AuthGuard } from './services/auth.guard';
import { LoginGuard } from './services/login.guard';

const routes: Route[] = [{
  path: '',
  redirectTo: 'menu',
  pathMatch: 'full',
},
{
  path: 'menu',
  component: MenuPageComponent
},
{
  path: 'channels',
  component: ChannelsPageComponent,
  canActivate: [AuthGuard]
},
{
  path: 'about',
  component: AboutComponent
},
{
  path: 'login',
  component: LoginPageComponent,
  canActivate: [LoginGuard]
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]

})

export class RoutingModue { }
