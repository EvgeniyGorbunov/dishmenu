import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrdersComponent } from './components/orders/orders.component';
import { DishComponent } from './components/dish/dish.component';
import { ModalComponent } from './components/modal/modal.component';
import { HeaderComponent } from './components/header/header.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { RoutingModue } from './routing.module';
import { ChannelsPageComponent } from './pages/chanenels/chanenels.component';
import { AboutComponent } from './pages/about/about.component';
import { LoginPageComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ChannelComponent } from './pages/chanenels/channel/channel.component';
import { LoopingRhumbusesSpinnerModule } from 'angular-epic-spinners';
import { DateComponent } from './pages/chanenels/date/date.component';
import { DatePipe, TimePipe } from './pipes/';
import { TvshowComponent } from './pages/chanenels/tvshow/tvshow.component';
import {
  TimeService,
  LoadingService,
  DataService,
  AuthService,
  LoginGuard,
  AuthGuard
} from './services';
import { ColorDirective } from './directives/color.directive';
import { ClickDirective } from './directives/click.directive';


@NgModule({
  declarations: [
    AppComponent,
    OrdersComponent,
    DishComponent,
    ModalComponent,
    HeaderComponent,
    MenuPageComponent,
    ChannelsPageComponent,
    AboutComponent,
    LoginPageComponent,
    ChannelComponent,
    DateComponent,
    DatePipe,
    TvshowComponent,
    TimePipe,
    ColorDirective,
    ClickDirective
  ],
  imports: [
    BrowserModule,
    RoutingModue,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    LoopingRhumbusesSpinnerModule,

  ],
  providers: [
    AuthService,
    AuthGuard,
    LoginGuard,
    DataService,
    LoadingService,
    TimeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
