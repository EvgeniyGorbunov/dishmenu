import { Component, OnInit } from '@angular/core';
import { Dish } from './models/dish.model';
import { LoadingService } from './services/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public isLoading: boolean;

  constructor(private loadingService: LoadingService) { }

  ngOnInit() {
    this.loadingService.loadingEvent.subscribe(res => {
      this.isLoading = res;
    });
  }
}
