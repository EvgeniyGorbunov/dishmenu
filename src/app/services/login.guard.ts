import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()

export class LoginGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate() {
    const isLogin = this.authService.isLogin;
    if (isLogin) {
      this.router.navigate(['menu']);
      return false;
    } else {
      return true;
    }
  }

}
