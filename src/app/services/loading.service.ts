import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class LoadingService {

  public loadingEvent = new Subject<boolean>();

  constructor() {

  }

  startLoading(): void {
    this.loadingEvent.next(true);
  }
  stopLoading(): void {
    setTimeout(() => {
      this.loadingEvent.next(false);
    }, 500);
  }
}
