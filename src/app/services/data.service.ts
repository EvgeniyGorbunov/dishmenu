import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Channels, BackendTvshows, Tvshow } from '../models/channels.model';
import { map } from 'rxjs/operators';

@Injectable()

export class DataService {

  private readonly URL = 'https://api.persik.by/v2/';

  constructor(private http: HttpClient) { }

  getChannels(): Observable<Channels> {
    return this.http.get<Channels>(this.URL.concat('content/channels'));
  }

  getTvshowsById(channelId: number): Observable<Tvshow[]> {
    return this.http.get<BackendTvshows>(this.URL.concat('epg/tvshows?channels[]=', channelId.toString()))
    .pipe(map(data => data.tvshows.items));
  }

}
