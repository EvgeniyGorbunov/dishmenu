import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Subject } from 'rxjs';

@Injectable()

export class TimeService {
  private readonly timeInterval = 1000;

  public timer = new Subject<number>();


  constructor() { }

  get currentTime(): number {
    return moment().unix();
  }

  startTimer(): void {
    setInterval(() => {
      this.timer.next(this.currentTime);
    }, this.timeInterval);
  }

}
