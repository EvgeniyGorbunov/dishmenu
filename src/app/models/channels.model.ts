
export interface Channel {
  age_rating: string;
  available: boolean;
  channel_id: number;
  current_tvshow_id: string;
  dvr_sec: number;
  genres: number[]; //Array<number>
  logo: string;
  name: string;
  priority: number;
  rank: number;
  stream_url: boolean;
}
export interface Channels {
  channels: Channel[];
}

export interface Tvshow {

  channel_id: number;
  date: string;
  deleted: string;
  start: number;
  stop: number;
  title: string;
  ts: number;
  tvshow_id: string;
  video_id: number;
}

export interface Tvshows {
  items: Tvshow[];
  total: number;
}

export interface BackendTvshows {
  tvshows: Tvshows;
}
