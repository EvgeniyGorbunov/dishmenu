import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Channel, Tvshow } from 'src/app/models/channels.model';
import { ChannelComponent } from './channel/channel.component';
import { LoadingService } from 'src/app/services/loading.service';
import * as moment from 'moment';
import { DateComponent } from './date/date.component';
import { TimeService } from 'src/app/services/time.service';

@Component({
  selector: 'app-channels-page',
  templateUrl: './chanenels.component.html',
  styleUrls: ['./chanenels.component.scss']
})

export class ChannelsPageComponent implements OnInit {



  public uniqueDates: string[] = [];
  public channels: Channel[] = [];
  public tvshows: Tvshow[] = [];
  public shows: Tvshow[] = [];
  public currentTime: number;

  constructor(private dataService: DataService,
    private loadingService: LoadingService,
    private timeService: TimeService) {
    this.loadingService.startLoading();
  }

  ngOnInit() {
    this.timeService.startTimer();
    this.currentTime = this.timeService.currentTime;
    this.timeService.timer.subscribe(time => {
      this.currentTime = time;
    });

    this.dataService.getChannels().subscribe(data => {
      this.channels = data.channels;
      ChannelComponent.activeId = this.channels[0].channel_id;
      this.onChannelSelected(this.channels[0].channel_id);
      this.loadingService.stopLoading();
    });

  }

  public onChannelSelected(channelId: number): void {
    this.loadingService.startLoading();
    this.dataService.getTvshowsById(channelId).subscribe(data => {
      this.tvshows = data;
      this.getUniqueDates();
      this.onDateChange(moment().format('YYYY-MM-DD'));
      this.loadingService.stopLoading();
    });
  }
  private getUniqueDates(): void {
    const dates = this.tvshows.map(item => item.date);
    this.uniqueDates = dates.filter((item, index, self) => self.indexOf(item) === index);
    DateComponent.activeDate = moment().format('YYYY-MM-DD');

  }
  public onDateChange(date: string): void {
    this.shows = this.tvshows.filter(item => item.date === date);

  }


}
