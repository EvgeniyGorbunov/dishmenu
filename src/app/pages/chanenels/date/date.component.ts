import { Component, Input, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})

export class DateComponent implements OnInit {
  static activeDate: string;

  @Input() date: string;
  @ViewChild('current', { static: false }) cur: ElementRef;
  @Output() dateEvent = new EventEmitter<string>();
  constructor() { }
  ngOnInit() {
    setTimeout(() => {
      if (this.isActive) {
        const element: HTMLDivElement = this.cur.nativeElement;
        element.scrollIntoView({ behavior: 'smooth' });
      }
    }, 100);
  }

  public get isActive(): boolean {
    return this.date === DateComponent.activeDate;
  }

  public setActiveDate(): void {
    DateComponent.activeDate = this.date;
    this.dateEvent.emit(this.date);
  }
}
