import { Component } from '@angular/core';
import { Dish } from 'src/app/models/dish.model';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuPageComponent {
  constructor() { }
  public dishes: Array<Dish> = [
    {
      id: 0,
      name: 'Котлеты',
      description: '',
      picture: '../assets/img/kotlet.jpg',
      price: 2.2
    },
    {
      id: 1,
      name: 'Овощное асорти',
      description: '',
      picture: '../assets/img/asorti.jpg',
      price: 3.2
    },
    {
      id: 2,
      name: 'Форель',
      description: '',
      picture: '../assets/img/forel.jpg',
      price: 7.25
    },
    {
      id: 3,
      name: 'Картошка',
      description: '',
      picture: '../assets/img/kartoshka.jpg',
      price: 2.2
    },
    {
      id: 4,
      name: 'Рыба в панировке',
      description: '',
      picture: '../assets/img/kruzhki.jpg',
      price: 6.24
    },
    {
      id: 5,
      name: 'Стэйк из говядины',
      description: '',
      picture: '../assets/img/steak.jpg',
      price: 12.2
    }
  ];

  public order: Array<Dish> = [];

  public isShowModal: boolean;
  public modalData: Dish;


  public onSelect(id: number) {
    const item: Dish = this.dishes.find(dish => dish.id === id);
    this.order.push(item);
    console.log(this.order);
  }
  public get isHaveOrder(): boolean {
    return this.order.length > 0;
  }
  public showDescription(dish: Dish): void {
    this.modalData = dish;
    this.isShowModal = true;
  }
  public onModalClose() {
    this.isShowModal = false;
  }
  public onDeleteItem(item: Dish): void {
    this.order.splice(this.order.indexOf(item), 1);
  }
}
