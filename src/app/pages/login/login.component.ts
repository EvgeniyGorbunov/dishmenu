import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginPageComponent implements OnInit {

  public name: FormControl;
  public password: FormControl;
  public userForm: FormGroup;

  constructor(
    private authService: AuthService, private router: Router
  ) { }

  ngOnInit() {
    this.createFormFields();
    this.createUserForm();
  }

  private createFormFields(): void {
    this.name = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(4)]);
  }

  private createUserForm(): void {
    this.userForm = new FormGroup({
      name: this.name,
      password: this.password
    });
  }

  public autorization(): void {
    if (this.userForm.valid) {
      this.authService.login(this.name.value, this.password.value);
      //this.authService.login(this.userForm.value.name)
      const isAuth = this.authService.isLogin;
      if (isAuth) {
        this.router.navigate(['channels']);
      }
    }

  }
}
